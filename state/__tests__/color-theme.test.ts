import {
  getBluePrintColorClassFrom,
  getThemeSuffixFrom,
  WPPColorThemes,
} from "../color-theme.state"

describe("getBluePrintColorClassFrom", () => {
  it("should return undefined for the light theme", () => {
    expect(getBluePrintColorClassFrom(WPPColorThemes.LIGHT)).toBeUndefined()
  })

  it("should return bp3-dark for the dark theme", () => {
    expect(getBluePrintColorClassFrom(WPPColorThemes.DARK)).toEqual("bp3-dark")
  })
})

describe("getThemeSuffixFrom", () => {
  it("should return the dark theme suffix for the dark theme", () => {
    expect(getThemeSuffixFrom(WPPColorThemes.DARK)).toEqual(WPPColorThemes.DARK)
  })
  it("should return the white theme suffix for the light theme", () => {
    expect(getThemeSuffixFrom(WPPColorThemes.LIGHT)).toEqual(
      WPPColorThemes.LIGHT
    )
  })
})
