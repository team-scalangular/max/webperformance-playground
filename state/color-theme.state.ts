import { atom } from "recoil"

enum WPPColorThemes {
  LIGHT = "light-theme",
  DARK = "dark-theme",
}

type BluePrintDarkThemeClassName = "bp3-dark" | undefined

interface AggregatedColorThemeInformation {
  themeSuffix: WPPColorThemes
  bluePrintClassName: BluePrintDarkThemeClassName
}

const colorThemeState = atom<WPPColorThemes>({
  key: "color-theme",
  default: WPPColorThemes.LIGHT,
})

function getBluePrintColorClassFrom(
  activeColorTheme: WPPColorThemes
): BluePrintDarkThemeClassName | undefined {
  return activeColorTheme === WPPColorThemes.DARK ? "bp3-dark" : undefined
}

function getThemeSuffixFrom(activeColorTheme: WPPColorThemes): WPPColorThemes {
  return activeColorTheme === WPPColorThemes.DARK
    ? WPPColorThemes.DARK
    : WPPColorThemes.LIGHT
}

export {
  WPPColorThemes,
  colorThemeState,
  getBluePrintColorClassFrom,
  getThemeSuffixFrom,
  type BluePrintDarkThemeClassName,
  type AggregatedColorThemeInformation,
}
