import { ReactElement } from "react"

function HomeAboutText(): ReactElement {
  return (
    <>
      This website aims to be a playground to test the impact of different
      web-performance-optimization methods, I had researched in the context of
      my Bachelor&apos;s Thesis, which was about &quot;performance optimizations
      in single-page-application&quot;
      <br />
      <br />
      On this page you can find different tabs with information and live tests
      for certain web performance optimization areas, like loop-performance,
      perceived-performance and monitoring tools. If you catch interest in the
      topic and want to learn more, you could give my thesis a try to learn more
      about the topic. (unfortunately it is only available in german) but you
      can find it{" "}
      <a
        target="_blank"
        rel="noopener noreferrer"
        href="https://www.shaker.eu/en/content/catalogue/index.asp?lang=en&ID=6&category=507"
      >
        here
      </a>
      .
      <br />
      <br />
      This page is developed by{" "}
      <a
        target="_blank"
        rel="noopener noreferrer"
        href="https://scalangular.com/maxe/about"
      >
        Maximilian Bieleke
      </a>
      . And currently is a side project of mine, so any updates come
      sporadically. If you want to be up to date in terms of the newest
      features, you can checkout the CHANGELOG which can be found in the{" "}
      <a
        target="_blank"
        rel="noopener noreferrer"
        href="https://gitlab.com/team-scalangular/max/webperformance-playground"
      >
        GitLab-repository.
      </a>
    </>
  )
}

export default HomeAboutText
