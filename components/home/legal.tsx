import { ReactElement } from "react"

function HomeLegalText(): ReactElement {
  return (
    <>
      This page is a subpage of the scalangular domain, which is run by my
      Friend{" "}
      <a
        target="_blank"
        rel="noopener noreferrer"
        href="https://scalangular.com/moritz"
      >
        Moritz Lindner
      </a>{" "}
      and{" "}
      <a
        target="_blank"
        rel="noopener noreferrer"
        href="https://scalangular.com/maxe/about"
      >
        me
      </a>{" "}
      you can find any Legal information regarding the privacy policy and legal
      information on our{" "}
      <a
        target="_blank"
        rel="noopener noreferrer"
        href="https://scalangular.com/legal"
      >
        main page
      </a>
      , with the addition, that the webperformance-playground will not collect
      any userdata.
    </>
  )
}

export default HomeLegalText
