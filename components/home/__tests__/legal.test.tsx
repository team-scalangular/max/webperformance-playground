import { shallow } from "enzyme"
import HomeLegalText from "../legal"

const SCALANGULAR_LINK = "https://scalangular.com/legal"

describe("legal text", () => {
  it("should reference the scalangular legal page in a link at least once", () => {
    const wrapper = shallow(<HomeLegalText />)
    expect(
      wrapper
        .find("a")
        .findWhere((linkElem) => linkElem.props().href === SCALANGULAR_LINK)
        .length
    ).toBeGreaterThanOrEqual(1)
  })
})
