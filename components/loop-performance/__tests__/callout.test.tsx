import { shallow } from "../../../__tests__/enzyme"
import Callout from "../callout"

describe("callout", () => {
  it("should render a callout with a textblock", () => {
    expect(shallow(<Callout />)).toMatchSnapshot()
  })
})
