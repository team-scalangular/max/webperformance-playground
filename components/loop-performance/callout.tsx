import { ReactElement } from "react"
import { Callout } from "@blueprintjs/core"

function LoopPerformanceCallout(): ReactElement {
  return (
    <div className="lp__callout">
      <Callout title="Disclaimer: Loop Performance" intent="primary">
        <p>
          The current page is all about loop performance. Here you can test
          different kind of javascript loops and functional iteration methods
          like (for, for in, map, reduce). You can configure yourself a testing
          scenario to see which loops are the most performant. The performance
          is measured only in terms of speed. Factors like memory usage etc. are
          currently not considered (which might chance in the future).
        </p>
      </Callout>
    </div>
  )
}

export default LoopPerformanceCallout
