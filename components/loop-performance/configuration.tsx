import { ReactElement } from "react"
import { Card, H5 } from "@blueprintjs/core"
import ConfigurationDetails from "./configuration-details"

function LoopPerformanceConfiguration(): ReactElement {
  return (
    <div className="lp__config">
      <Card elevation={2}>
        <H5>Configure Testing Scenario</H5>
        <p>
          Here you can configure the parameters for the loop performance tests.{" "}
          <br />
          Available options are the type of loops to compare and the count of
          iterations for each loop.
        </p>
        <ConfigurationDetails />
      </Card>
    </div>
  )
}

export default LoopPerformanceConfiguration
