import { ReactElement } from "react"
import Select from "react-select"
import makeAnimated from "react-select/animated"
import { Button, Classes, Label, NumericInput } from "@blueprintjs/core"
import useForm from "@hooks/use-form"
import {
  inputsAreValidLoopOptions,
  validate,
} from "@utils/loop-performance/lp-validators"
import { LoopConfigKey } from "@utils/loop-performance/lp-types"
import {
  createMultiSelectOptions,
  initialLoopConfigValues,
  toLoopArray,
} from "@utils/loop-performance/lp-form-handler"

// TODO test
function ConfigurationDetails(): ReactElement {
  const runPerformanceTest = () => console.log("login")

  const { values, errors, handleChange, handleSubmit } = useForm(
    runPerformanceTest,
    validate,
    initialLoopConfigValues()
  )

  const buildEventAndHandleInputChange = (newCount: number) => {
    handleChange({
      target: {
        name: "iterationCount" as LoopConfigKey,
        value: newCount,
      },
    })
  }

  const buildEventAndHandleSelectChange = (selectedLoops: unknown) => {
    if (inputsAreValidLoopOptions(selectedLoops)) {
      handleChange({
        target: {
          name: "loopKind" as LoopConfigKey,
          value: selectedLoops.map(toLoopArray),
        },
      })
    }
  }

  return (
    <form id="lp-config" onSubmit={handleSubmit}>
      <div className="lp__detailed-config">
        <Label className="lp__select-label">
          Loops to test:
          <Select
            instanceId={"loop-select"}
            closeMenuOnSelect={false}
            options={createMultiSelectOptions()}
            components={makeAnimated()}
            placeholder="Select Loops to Measure..."
            className={`lp-multi-select ${errors.loopKind && "ErrorClass"}`}
            classNamePrefix="lp"
            onChange={buildEventAndHandleSelectChange}
            isMulti
          />
        </Label>
        <Label>
          Count of iterations:
          <NumericInput
            className={`lp__iteration-input ${
              errors.iterationCount && "ErrorClass"
            }`}
            value={values.iterationCount}
            majorStepSize={100}
            stepSize={10}
            minorStepSize={1}
            onValueChange={buildEventAndHandleInputChange}
          />
        </Label>
      </div>
      <div className="lp__submit-button">
        <Button
          type="submit"
          form="lp-config"
          text="Start Benchmark"
          className={Classes.BUTTON}
        />
      </div>
    </form>
  )
}

export default ConfigurationDetails
