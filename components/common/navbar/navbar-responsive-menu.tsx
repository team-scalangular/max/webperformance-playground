import { ReactElement, useState } from "react"
import { Popover2 } from "@blueprintjs/popover2"
import { Button, Classes, Menu, MenuItem } from "@blueprintjs/core"
import navigationItems from "@utils/navigation/navigation-items"
import Link from "next/link"

function NavbarResponsiveMenu(): ReactElement {
  const [isOpened, toggleOpened] = useState(false)

  const responsiveMenu: ReactElement = (
    <Menu>
      {navigationItems.map((navItem, index) => (
        <Link key={`menu-item-${index}`} href={navItem.href || "/"} passHref>
          <MenuItem icon={navItem.icon} text={navItem.text} />
        </Link>
      ))}
    </Menu>
  )

  return (
    <div className="navbar__link-menu">
      <Popover2
        content={responsiveMenu}
        placement="bottom-end"
        isOpen={isOpened}
        onInteraction={toggleOpened}
      >
        <Button
          className={Classes.MINIMAL}
          icon={isOpened ? "cross" : "menu"}
        />
      </Popover2>
    </div>
  )
}

export default NavbarResponsiveMenu
