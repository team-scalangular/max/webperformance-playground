import { ReactElement } from "react"
import { Button, Classes } from "@blueprintjs/core"
import Link from "next/link"
import useInnerWidth from "@hooks/use-inner-width"

function NavbarTitleButton(): ReactElement {
  const innerWidth = useInnerWidth()

  return (
    <Link href="/" passHref>
      <Button
        className={Classes.MINIMAL}
        text={
          innerWidth && innerWidth < 600
            ? "WP Playground"
            : "Webperformance Playground"
        }
      />
    </Link>
  )
}

export default NavbarTitleButton
