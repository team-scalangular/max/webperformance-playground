import { ReactElement } from "react"
import {
  Alignment,
  Navbar,
  NavbarDivider,
  NavbarGroup,
  NavbarHeading,
} from "@blueprintjs/core"
import NavbarLinkList from "./navbar-link-list"
import NavbarResponsiveMenu from "./navbar-responsive-menu"
import NavbarThemeSwitch from "./navbar-theme-switch"
import NavbarTitleButton from "./navbar-title-button"

function WPPlaygroundNavbar(): ReactElement {
  return (
    <header>
      <Navbar className="navbar__nav-element">
        <NavbarGroup align={Alignment.LEFT}>
          <NavbarHeading>
            <NavbarTitleButton />
          </NavbarHeading>
          <NavbarDivider />
          <NavbarThemeSwitch />
          <NavbarLinkList />
        </NavbarGroup>
        <NavbarGroup align={Alignment.RIGHT}>
          <NavbarResponsiveMenu />
        </NavbarGroup>
      </Navbar>
    </header>
  )
}

export default WPPlaygroundNavbar
