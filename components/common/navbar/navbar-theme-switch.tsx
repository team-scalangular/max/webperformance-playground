import { ReactElement } from "react"
import { Alignment, Switch } from "@blueprintjs/core"
import { useRecoilState } from "recoil"
import { colorThemeState, WPPColorThemes } from "@state/color-theme.state"

function NavbarThemeSwitch(): ReactElement {
  const [activeTheme, setActiveTheme] = useRecoilState(colorThemeState)

  return (
    <Switch
      className="navbar__theme-switch"
      alignIndicator={Alignment.RIGHT}
      innerLabelChecked="Light Mode"
      innerLabel="Dark Mode"
      inline={true}
      checked={activeTheme === WPPColorThemes.LIGHT}
      onChange={() =>
        setActiveTheme(
          activeTheme === WPPColorThemes.DARK
            ? WPPColorThemes.LIGHT
            : WPPColorThemes.DARK
        )
      }
    />
  )
}

export default NavbarThemeSwitch
