import { ReactElement } from "react"
import { Button, Classes, NavbarDivider } from "@blueprintjs/core"
import navigationItems from "@utils/navigation/navigation-items"
import Link from "next/link"

function NavbarLinkList(): ReactElement {
  return (
    <>
      <NavbarDivider className="navbar__link-list" />
      <div className="navbar__link-list">
        {navigationItems.map((navItem, index) => (
          <Link
            key={`navigation-link-${index}`}
            href={navItem.href || "/"}
            passHref
          >
            <Button
              className={Classes.MINIMAL}
              icon={navItem.icon}
              text={navItem.text}
            />
          </Link>
        ))}
      </div>
    </>
  )
}

export default NavbarLinkList
