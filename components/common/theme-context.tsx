import { ReactElement } from "react"
import useThemeContext from "@hooks/use-theme-context"

interface ThemeContextProps {
  children: ReactElement | ReactElement[]
}

function ThemeContext({ children }: ThemeContextProps): ReactElement {
  const { bluePrintClassName } = useThemeContext()
  return (
    <div className={bluePrintClassName && `${bluePrintClassName}`}>
      {children}
    </div>
  )
}

export default ThemeContext
