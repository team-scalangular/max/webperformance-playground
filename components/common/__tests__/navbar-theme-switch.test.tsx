import { WPPColorThemes } from "../../../state/color-theme.state"
import { useRecoilState } from "recoil"
import { shallow, ShallowWrapper } from "enzyme"
import NavbarThemeSwitch from "../navbar/navbar-theme-switch"
import { Switch } from "@blueprintjs/core"

jest.mock("recoil")
const setActiveTheme = jest.fn()
const useRecoilStateMock = jest
  .mocked(useRecoilState)
  .mockReturnValue([WPPColorThemes.DARK, setActiveTheme])

describe("navbar theme switch", () => {
  let wrapper: ShallowWrapper<typeof NavbarThemeSwitch>

  it("should render a switch with the correct labels", () => {
    wrapper = shallow(<NavbarThemeSwitch />)
    expect(wrapper).toMatchSnapshot()
  })

  it("should set the color theme to white on click", () => {
    wrapper = shallow(<NavbarThemeSwitch />)
    wrapper.find(Switch).simulate("change")
    expect(setActiveTheme).toHaveBeenCalledWith(WPPColorThemes.LIGHT)
  })

  describe("given light theme is selected", () => {
    it("should set the color theme to dark on click", () => {
      useRecoilStateMock.mockReturnValueOnce([
        WPPColorThemes.LIGHT,
        setActiveTheme,
      ])
      wrapper = shallow(<NavbarThemeSwitch />)
      wrapper.find(Switch).simulate("change")
      expect(setActiveTheme).toHaveBeenCalledWith(WPPColorThemes.DARK)
    })
  })
})
