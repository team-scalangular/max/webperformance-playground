import useThemeContext from "@hooks/use-theme-context"
import { WPPColorThemes } from "@state/color-theme.state"
import { ShallowWrapper } from "enzyme"
import ThemeContext from "../theme-context"
import { shallow } from "../../../__tests__/enzyme"

jest.mock("@hooks/use-theme-context")
const useThemeContextMock = jest.mocked(useThemeContext).mockReturnValue({
  themeSuffix: WPPColorThemes.LIGHT,
  bluePrintClassName: undefined,
})

describe("theme context", () => {
  let wrapper: ShallowWrapper<typeof ThemeContext>

  beforeEach(() => {
    wrapper = shallow(
      <ThemeContext>
        <div />
      </ThemeContext>
    )
  })

  it("should not have a className prop by default", () => {
    expect(wrapper.prop("className")).toEqual(undefined)
  })

  it("should pass the children from the props in the tsx", () => {
    const childType = wrapper.children().at(0).type()

    expect(wrapper.children()).toHaveLength(1)
    expect(childType).toEqual("div")
  })

  describe("given the dark theme is selected", () => {
    it("should add the bluePrint classname in the props", () => {
      useThemeContextMock.mockReturnValue({
        themeSuffix: WPPColorThemes.DARK,
        bluePrintClassName: "bp3-dark",
      })

      wrapper = shallow(
        <ThemeContext>
          <div />
        </ThemeContext>
      )

      expect(wrapper.prop("className")).toEqual("bp3-dark")
    })
  })
})
