import { mount, ReactWrapper } from "../../../__tests__/enzyme"
import NavbarResponsiveMenu from "../navbar/navbar-responsive-menu"
import { Button, MenuItem } from "@blueprintjs/core"
import { Popover2 } from "@blueprintjs/popover2"
import { act } from "react-dom/test-utils"
import navigationItems from "../../../utils/navigation/navigation-items"

describe("navbar responsive menu", () => {
  let wrapper: ReactWrapper<typeof NavbarResponsiveMenu>

  beforeEach(() => {
    wrapper = mount(<NavbarResponsiveMenu />)
  })

  it("should render a popover including the menu", () => {
    expect(wrapper).toMatchSnapshot()
  })

  it("should have the menu icon as default if not opened", () => {
    const menuIcon = wrapper.find(Button).first().props().icon

    expect(menuIcon).toEqual("menu")
  })

  describe("given the menu item is open", () => {
    it("should display the close icon", async () => {
      wrapper.find(Popover2).simulate("click")

      const menuIcon = wrapper.find(Button).first().props().icon
      expect(menuIcon).toEqual("cross")

      await cleanup()
    })

    it("should render all menu options", async () => {
      wrapper.find(Popover2).simulate("click")
      const listElem = wrapper.find(".bp3-menu").first().children()

      expect(listElem).toHaveLength(navigationItems.length)
      await cleanup()
    })

    it.each(
      navigationItems.map((item, index) => [item.icon, item.text, index])
    )(
      'should render the menu with the correct icons and text for element at "$index"',
      async (icon, text, index) => {
        wrapper.find(Popover2).simulate("click")
        const listElem = wrapper
          .find(MenuItem)
          .at(index as number)
          .props()

        expect(listElem.text).toEqual(text)
        expect(listElem.icon).toEqual(icon)
        await cleanup()
      }
    )
  })
})

/**
 * method to fix the warning from popper <br>
 * [see issue]{@link https://github.com/floating-ui/floating-ui/issues/1520#issuecomment-1021741758}
 */
async function cleanup(): Promise<void> {
  await act(async () => await undefined)
}
