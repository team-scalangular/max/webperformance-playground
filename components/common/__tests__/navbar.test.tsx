import { shallow } from "../../../__tests__/enzyme"
import WPPlaygroundNavbar from "../navbar/navbar"

describe("navbar", () => {
  it("should render a navbar with linklist and responsive menu", () => {
    expect(shallow(<WPPlaygroundNavbar />)).toMatchSnapshot()
  })
})
