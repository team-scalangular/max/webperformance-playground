import NavbarTitleButton from "../navbar/navbar-title-button"
import { shallow } from "../../../__tests__/enzyme"
import { Button } from "@blueprintjs/core"
import useInnerWidth from "@hooks/use-inner-width"
import Link from "next/link"
import { ShallowWrapper } from "enzyme"

jest.mock("@hooks/use-inner-width")
const innerWidthMock = jest.mocked(useInnerWidth).mockReturnValue(900)

describe("navbar title button", () => {
  let wrapper: ShallowWrapper<typeof NavbarTitleButton>

  beforeEach(() => {
    jest.clearAllMocks()
    wrapper = shallow(<NavbarTitleButton />)
  })

  it("should link to the home page", () => {
    const href = wrapper.find(Link).first().props().href

    expect(href).toEqual("/")
  })

  it('should use "webperformance playground" as title', () => {
    const titleText = wrapper.find(Button).first().props().text

    expect(titleText).toEqual("Webperformance Playground")
  })

  describe("given the window width is less than 600", () => {
    it("should render a smaller title", () => {
      innerWidthMock.mockReturnValueOnce(599)
      const wrapper = shallow(<NavbarTitleButton />)
      const titleText = wrapper.find(Button).first().props().text

      expect(titleText).toEqual("WP Playground")
    })
  })
})
