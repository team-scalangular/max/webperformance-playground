import { shallow, ShallowWrapper } from "../../../__tests__/enzyme"
import NavbarLinkList from "../navbar/navbar-link-list"
import navigationItems from "../../../utils/navigation/navigation-items"
import { Button } from "@blueprintjs/core"

describe("navbar link list", () => {
  let wrapper: ShallowWrapper<typeof NavbarLinkList>

  beforeEach(() => {
    wrapper = shallow(<NavbarLinkList />)
  })

  it("should render a navbar divider with the buttons", () => {
    expect(wrapper).toMatchSnapshot()
  })

  it.each(navigationItems.map((item, index) => [item.icon, item.text, index]))(
    'should render the menu with the correct icons and text for element at "$index"',
    async (icon, text, index) => {
      const listElem = wrapper
        .find(Button)
        .at(index as number)
        .props()

      expect(listElem.text).toEqual(text)
      expect(listElem.icon).toEqual(icon)
    }
  )
})
