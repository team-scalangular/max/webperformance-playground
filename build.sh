#!/usr/bin/env sh

echo "Begin docker build process..."
echo "Setting up yarn to use node_modules..."

rm -rf .next
cat .yarnrc.yml >> .temp.yml
echo "nodeLinker: node-modules" >> .yarnrc.yml

echo "Starting docker build process..."

docker build -t "wp-test" .

echo "Docker build completed running cleanup"

rm -rf .next
rm .yarnrc.yml
cat .temp.yml >> .yarnrc.yml
rm .temp.yml

