import {
  AggregatedColorThemeInformation,
  colorThemeState,
  getBluePrintColorClassFrom,
  getThemeSuffixFrom,
  WPPColorThemes,
} from "@state/color-theme.state"
import { useRecoilValue } from "recoil"

function useThemeContext(): AggregatedColorThemeInformation {
  const activeColorTheme = useRecoilValue(colorThemeState)
  const themeSuffix: WPPColorThemes = getThemeSuffixFrom(activeColorTheme)
  const bluePrintClassName = getBluePrintColorClassFrom(activeColorTheme)
  return { themeSuffix, bluePrintClassName }
}

export default useThemeContext
