import React, { ReactElement } from "react"
import useForm from "@hooks/use-form"
import { mount } from "../../__tests__/enzyme"

const mockCallback = jest.fn()
const mockValidator = jest.fn().mockReturnValue({})
const setValues = jest.fn()

function MockFormComponent(): ReactElement {
  const { values, handleChange, handleSubmit } = useForm(
    mockCallback,
    mockValidator,
    { input: "hello" }
  )

  return (
    <form onSubmit={handleSubmit}>
      <input value={values.input} onChange={handleChange} />
    </form>
  )
}

describe("use form", () => {
  beforeEach(() => jest.clearAllMocks())

  it("should set the initial values if passed", () => {
    const useStateSpy = jest.spyOn(React, "useState")

    mount(<MockFormComponent />)

    expect(useStateSpy).toHaveBeenCalledTimes(3)
    expect(useStateSpy).toHaveBeenNthCalledWith(1, { input: "hello" })
    expect(useStateSpy).toHaveBeenNthCalledWith(2, {})
    expect(useStateSpy).toHaveBeenNthCalledWith(3, false)
  })

  it("should set the correct values on change event", () => {
    jest.spyOn(React, "useState").mockReturnValueOnce([{}, setValues])

    const wrapper = mount(<MockFormComponent />)
    wrapper.find("input").first().simulate("change")

    expect(setValues).toHaveBeenCalledTimes(1)
    expect(setValues).toHaveBeenCalledWith(expect.any(Function))
  })

  it("should call the provided callback function on valid submit", () => {
    const wrapper = mount(<MockFormComponent />)
    wrapper.find("form").first().simulate("submit")

    expect(mockValidator).toHaveBeenCalledWith({ input: "hello" })
    expect(mockCallback).toHaveBeenCalled()
  })

  describe("given the form is includes errors", () => {
    it("should not call the callback function", () => {
      mockValidator.mockReturnValueOnce({ input: "some error" })
      const wrapper = mount(<MockFormComponent />)
      wrapper.find("form").first().simulate("submit")

      expect(mockCallback).toHaveBeenCalledTimes(0)
    })
  })
})
