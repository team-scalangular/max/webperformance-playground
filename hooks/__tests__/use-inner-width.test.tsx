import React, { ReactElement } from "react"
import useInnerWidth from "../use-inner-width"
import { mount } from "enzyme"

const addEventListenerMock = jest.spyOn(window, "addEventListener")
const removeEventListenerMock = jest.spyOn(window, "removeEventListener")
const setInnerWidth = jest.fn()
const mockWidth = 900

Object.defineProperty(window, "innerWidth", {
  writable: true,
  configurable: true,
  value: mockWidth,
})

function MockEffectComponent(): ReactElement {
  const innerWidth = useInnerWidth()
  return <div>{innerWidth}</div>
}

describe("use inner width", () => {
  beforeEach(() => jest.clearAllMocks())

  it("should set a width state initially", () => {
    jest.spyOn(React, "useState").mockReturnValue([innerWidth, setInnerWidth])
    mount(<MockEffectComponent />)
    expect(setInnerWidth).toHaveBeenCalledWith(mockWidth)
  })

  it("should create an event listener on mount", () => {
    mount(<MockEffectComponent />)
    expect(addEventListenerMock).toHaveBeenCalledWith(
      "resize",
      expect.any(Function)
    )
  })

  describe("given the component is destroyed", () => {
    it("should remove the event listener", () => {
      const wrapper = mount(<MockEffectComponent />)
      wrapper.unmount()
      expect(removeEventListenerMock).toHaveBeenCalledWith(
        "resize",
        expect.any(Function)
      )
    })
  })
})
