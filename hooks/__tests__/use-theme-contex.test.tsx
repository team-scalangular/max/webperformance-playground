import { useRecoilValue } from "recoil"
import { WPPColorThemes } from "@state/color-theme.state"
import { ReactElement } from "react"
import useThemeContext from "@hooks/use-theme-context"
import { ReactWrapper } from "enzyme"
import { mount } from "../../__tests__/enzyme"

function MockComponent(): ReactElement {
  const { themeSuffix, bluePrintClassName } = useThemeContext()
  return (
    <div>
      {themeSuffix}
      {bluePrintClassName}
    </div>
  )
}

jest.mock("recoil")
const useRecoilValueMock = jest
  .mocked(useRecoilValue)
  .mockReturnValue(WPPColorThemes.LIGHT)

describe("use theme context", () => {
  let wrapper: ReactWrapper<typeof MockComponent>

  it.each`
    theme            | correspondingSuffix
    ${"light theme"} | ${WPPColorThemes.LIGHT}
    ${"dark theme "} | ${WPPColorThemes.DARK}
  `(
    "should return the correct theme suffix for '$theme‘",
    ({ correspondingSuffix }) => {
      if (correspondingSuffix === WPPColorThemes.DARK) {
        useRecoilValueMock.mockReturnValueOnce(WPPColorThemes.DARK)
      }
      wrapper = mount(<MockComponent />)
      const children = wrapper.children()
      expect(children.at(0).text()).toContain(correspondingSuffix)
    }
  )

  it("should not include the bluePrint dark theme class by default", () => {
    wrapper = mount(<MockComponent />)
    const children = wrapper.children()
    expect(children.at(0).text()).not.toContain("bp3-dark")
  })

  describe("given the dark-theme is selected", () => {
    it("should also return the bluePrint dark theme class", () => {
      useRecoilValueMock.mockReturnValueOnce(WPPColorThemes.DARK)
      wrapper = mount(<MockComponent />)
      const children = wrapper.children()
      expect(children.at(0).text()).toContain("bp3-dark")
    })
  })
})
