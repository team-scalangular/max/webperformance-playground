import { useEffect, useState } from "react"

function useInnerWidth(): number | null {
  const [innerWidth, setInnerWidth] = useState<number | null>(null)

  useEffect(() => {
    setInnerWidth(window.innerWidth)
    window.addEventListener("resize", () => setInnerWidth(window.innerWidth))
    return () =>
      window.removeEventListener("resize", () =>
        setInnerWidth(window.innerWidth)
      )
  }, [])

  return innerWidth
}

export default useInnerWidth
