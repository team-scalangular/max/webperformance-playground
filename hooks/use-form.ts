import { SyntheticEvent, useEffect, useState } from "react"
import { EmptyObject } from "@utils/common/types"

const useForm = <T, E>(
  onValidSubmit: () => void,
  validate: (values: T | EmptyObject) => E,
  initialValues?: T
) => {
  const [values, setValues] = useState<T | EmptyObject>(initialValues ?? {})
  const [errors, setErrors] = useState<E | EmptyObject>({})
  const [isSubmitting, setIsSubmitting] = useState(false)

  useEffect(() => {
    if (Object.keys(errors).length === 0 && isSubmitting) {
      onValidSubmit()
    }
  }, [errors])

  const handleSubmit = (event: SyntheticEvent) => {
    if (event) event.preventDefault()
    setErrors(validate(values))
    setIsSubmitting(true)
  }

  // @ts-ignore: Abstract form event could be refactored at some time
  // so that T is always record of string, T
  const handleChange = (event) => {
    setValues((values) => ({
      ...values,
      [event.target.name]: event.target.value,
    }))
  }

  return {
    handleChange,
    handleSubmit,
    values,
    errors,
  }
}

export default useForm
