# Webperformance Playground

This project was generated using `yarn create next-app`.
It follows the common structure of Next-Applications.

`Webperformance Playground` is a next.js spa which aims to showcase the impact of different webperformance optimization methods as well as different monitoring options for web performance, which I have researched in the course of my bachelor's thesis about performance optimization in single page applications, which can be found [here](http://www.shaker.eu/en/content/catalogue/index.asp?lang=en&ID=6&category=507).

## Environments

- Production: https://wpp.scalangular.com (currently not deployed)

## Get started

### Prerequisites

To get the project run locally you will need to Install Node.js on your system. (LTS Recommendet)  
This project uses yarn with the yarn plug'n'play mode for the local development.  
Therefore, it is recommended to have 3.2.x as local yarn version installed (as mentioned in the package json).
I also use husky and lint-staged to verify my commits doesn't break the lint stages this however should be installed automatically when running yarn install later on.

### Clone the repo

```shell
git clone git@gitlab.com:team-scalangular/max/webperformance-playground.git
cd webperformance-playground
```

### Install npm packages

Install the `pnp` packages described in the `package.json`:

```shell
yarn install
```

**Note: with yarn pnp there will be no node_module directory. All your dependencies are installed as .zip files in the .yarn/cache directory**  
you can verify that everything is working as expected by running:

```shell
yarn dev
```

The project will be served to [http://localhost:3000](http://localhost:3000)

#### npm scripts

These are the most useful commands defined in `package.json`:

- `yarn dev` - serves the project to localhost:3000
- `yarn build` - builds the application. (I currently use nex.js's experimental outputStandalone mode for smaller image sizes)
- `yarn test | lint` - for linting and testing the application (run test:coverage to get a coverage report)

### Build and Deployment (CI/CD)

The project uses the GitLab CI/CD Features. To view the Stages and Configurations
you can look inside the ".gitlab-ci.yml". <br>

#### Continues Integration

For the CI Part the project is tested in 2 Stages including:

- Linting (consisting of: next lint, prettier check and tsc --noEmit)
- Testing (consists of: test:coverage where the coverage is compared to the main coverage on the merge request)
- Build (dependent of lint and test -> builds the project as a docker image and pushes it to the gitlab registry using kaniko)

#### Continues Deployment

The app is deployed to a Portainer Docker Swarm cluster.
I use deployment via git tag for:

- Release (Prod) - Tag: Release_x.x.x (x.x.x -> semver regex)

For the deployment a docker image is created which is pushed to the Gitlab registry.
Afterwards the Portainer server gets notified via curl Request, to update its stack.

To keep the docker image with next.js as small as possible I currently use the experimental outputStandalone mode.
Which currently doesn't seem to support pnp modules. Therefore I decided to temporary opt-out of the usage of pnp for the build process only. This is pretty easy to and is also covered a build script which can simply can be runned to make local builds.

You can locally test the deployment, if you have docker installed on your machine. <br>

To build the docker image use the predefined shell script by running:

```bash
sh build.sh
```

This will automatically switch to node_modules as nodeLinker and will clean up the change after the execution so everything should stay normal for your local development.

To then run the image use:

```bash
docker run -p 3000:3000 wp-test
```

--> This runs the image on localhost:3000 <br>

## Dependency Updates

To see the outdated modules and do updates use the upgrade-interactive plugin defined in the .yarnrc.yml by running:

```bash
yarn upgrade-interactive
```
