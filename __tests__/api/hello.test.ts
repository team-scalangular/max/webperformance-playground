import { createMocks } from "node-mocks-http"
import handler from "../../pages/api/hello"

// Example on how to test the api routes later on
describe("hello ts", () => {
  it("should return 200 with john doe", () => {
    const { req, res } = createMocks({
      method: "GET",
    })

    handler(req, res)
    expect(res._getStatusCode()).toEqual(200)
    expect(JSON.parse(res._getData())).toMatchObject({ name: "John Doe" })
  })
})
