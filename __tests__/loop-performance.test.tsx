import { shallow } from "./enzyme"
import LoopPerformance from "../pages/loop-performance"
import { WPPColorThemes } from "@state/color-theme.state"
import useThemeContext from "@hooks/use-theme-context"

jest.mock("recoil")
jest.mock("@hooks/use-theme-context")

describe("loop performance page", () => {
  it("should render the loop performance page", () => {
    jest.mocked(useThemeContext).mockReturnValueOnce({
      themeSuffix: WPPColorThemes.LIGHT,
      bluePrintClassName: undefined,
    })

    expect(shallow(<LoopPerformance />)).toMatchSnapshot()
  })

  it.each`
    theme            | themeSuffix
    ${"light theme"} | ${WPPColorThemes.LIGHT}
    ${"dark theme"}  | ${WPPColorThemes.DARK}
  `("should apply the correct theme suffix for '$theme'", ({ themeSuffix }) => {
    jest.mocked(useThemeContext).mockReturnValueOnce({
      themeSuffix,
      bluePrintClassName: undefined,
    })

    const wrapper = shallow(<LoopPerformance />)
    expect(
      wrapper
        .find("section")
        .first()
        .props()
        .className?.includes(`lp__callout-section--${themeSuffix}`)
    ).toBeTruthy()
  })
})
