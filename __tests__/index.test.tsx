import Home from "../pages"
import { shallow } from "./enzyme"
import { ShallowWrapper } from "enzyme"
import HomeLegalText from "../components/home/legal"
import { useRecoilValue } from "recoil"
import { WPPColorThemes } from "@state/color-theme.state"

jest.mock("recoil")
jest.mocked(useRecoilValue).mockReturnValue(WPPColorThemes.DARK)

describe("index", () => {
  let wrapper: ShallowWrapper<typeof Home>

  beforeEach(() => {
    wrapper = shallow(<Home />)
  })

  it("should render the about and legal text", () => {
    expect(wrapper).toMatchSnapshot()
  })

  it("should include the legal text on the main page", () => {
    expect(wrapper.find(HomeLegalText).first()).toBeTruthy()
  })
})
