# Changelog

All notable changes to this project will be documented in this file.  
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

This section must stay on top of the changelog and contain the newest updates which are not yet
part of any release. Once a release is created, rename this section to the release number/name and
reinstantiate a new, blank "Unreleased" section at the top of the changelog.

### Added

- A simple navbar to present the page title and the navigation options
- Blueprint js as the main ui component library
- A home page with an about text and legal information
- The ability for changing the color mode between dark and white

### Changed

### Fixed

## 0.1.0 - 2022.03.10

`Webperformance Playground` is a next.js website which aims to showcase the impact of different webperformance optimization methods as well as different monitoring options for web performance, which I have researched in the course of my bachelor's thesis about performance optimization in single page applications, which can be found [here](http://www.shaker.eu/en/content/catalogue/index.asp?lang=en&ID=6&category=507).

### Added

- A next.js app with basic setup for testing and linting, which renders a default div with text
