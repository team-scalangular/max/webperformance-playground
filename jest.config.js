// eslint-disable-next-line @typescript-eslint/no-var-requires
const nextJest = require("next/jest")

const createJestConfig = nextJest({
  dir: "./",
})

const customJestConfig = {
  moduleDirectories: ["node_modules", "<rootDir>/"],
  roots: ["<rootDir>"],
  testEnvironment: "jest-environment-jsdom",
  setupFiles: ["<rootDir>/__tests__/enzyme.ts"],
  testPathIgnorePatterns: ["enzyme.ts"],
  snapshotSerializers: ["enzyme-to-json/serializer"],
  coverageReporters: ["text", "text-summary", "lcov", "cobertura"],
  moduleNameMapper: {
    "@utils/(.*)": "<rootDir>/utils/$1",
    "@state/(.*)": "<rootDir>/state/$1",
    "@hooks/(.*)": "<rootDir>/hooks/$1",
  },
  collectCoverageFrom: [
    "pages/**/*.{ts,tsx}",
    "components/**/*.{ts,tsx}",
    "utils/**/*.{ts,tsx}",
    "state/**/*.{ts,tsx}",
    "hooks/**/*.{ts,tsx}",
    "!<rootDir>/node_modules/",
  ],
  coverageThreshold: {
    global: {
      statements: 70,
      branches: 70,
      functions: 70,
      lines: 70,
    },
  },
}

module.exports = createJestConfig(customJestConfig)
