import { ReactElement } from "react"
import useThemeContext from "@hooks/use-theme-context"
import LoopPerformanceCallout from "../components/loop-performance/callout"
import LoopPerformanceConfiguration from "../components/loop-performance/configuration"

function LoopPerformance(): ReactElement {
  const { themeSuffix } = useThemeContext()

  return (
    <main className={`lp__main lp__main--${themeSuffix}`}>
      <section
        className={`lp__callout-section lp__callout-section--${themeSuffix}`}
      >
        <LoopPerformanceCallout />
        <LoopPerformanceConfiguration />
      </section>
    </main>
  )
}

export default LoopPerformance
