import type { NextPage } from "next"
import HomeAboutText from "../components/home/about"
import HomeLegalText from "../components/home/legal"
import useThemeContext from "@hooks/use-theme-context"

const Home: NextPage = () => {
  const { themeSuffix } = useThemeContext()
  return (
    <main>
      <section className={`home__intro home__intro--${themeSuffix}`}>
        <div
          className={`home__welcome-wrapper home__welcome-wrapper--${themeSuffix}`}
        >
          <h1>Welcome to the Webperformance Playground</h1>
          <h2 className="home__subheading">About</h2>
          <p className="home__welcome-subtext">
            <HomeAboutText />
          </p>
          <h2 className="home__subheading">Legal Information</h2>
          <p className="home__welcome-subtext">
            <HomeLegalText />
          </p>
        </div>
      </section>
    </main>
  )
}

export default Home
