import "../styles/_index.scss"
import type { AppProps } from "next/app"
import { FocusStyleManager } from "@blueprintjs/core"
import { RecoilRoot } from "recoil"
import WPPlaygroundNavbar from "../components/common/navbar/navbar"
import ThemeContext from "../components/common/theme-context"

function WPPlayground({ Component, pageProps }: AppProps) {
  FocusStyleManager.onlyShowFocusOnTabs()
  return (
    <RecoilRoot>
      <ThemeContext>
        <WPPlaygroundNavbar />
        <Component {...pageProps} />
      </ThemeContext>
    </RecoilRoot>
  )
}

export default WPPlayground
