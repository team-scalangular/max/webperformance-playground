import { IconName, MaybeElement } from "@blueprintjs/core"

interface WPPNavigationItems {
  icon: IconName | MaybeElement
  text: string
  href?: string
}

const navigationItems: WPPNavigationItems[] = [
  { icon: "function", text: "Loop Performance", href: "/loop-performance" },
  { icon: "eye-open", text: "Perceived Performance" },
  { icon: "build", text: "Measurement Tools" },
]

export default navigationItems
