enum Loop {
  FOR = "for loop",
  FOR_IN = "for in loop",
  FOR_OF = "for of loop",
  FOR_EACH = "forEach loop",
  MAP = "map loop",
  WHILE = "while loop",
  DO_WHILE = "do while loop",
}

type LoopKey = keyof typeof Loop

type IterationCount = number

interface LoopOption {
  value: LoopKey
  label: Loop
}

interface LoopMeasurementConfig {
  loopKind: Loop[]
  iterationCount: IterationCount
}

type LoopConfigKey = keyof LoopMeasurementConfig

enum LoopConfigValidators {
  MIN_TWO = "Please choose at least two loop",
  MIN_100_ITERATIONS = "Please choose at least 100 Iterations for the Test",
}

type LoopErrors = Record<LoopConfigKey, LoopConfigValidators>

export {
  Loop,
  LoopConfigValidators,
  type IterationCount,
  type LoopMeasurementConfig,
  type LoopOption,
  type LoopKey,
  type LoopConfigKey,
  type LoopErrors,
}
