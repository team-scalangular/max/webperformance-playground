import {
  Loop,
  LoopKey,
  LoopMeasurementConfig,
  LoopOption,
} from "@utils/loop-performance/lp-types"

const defaultIterationCount = 10000

function createMultiSelectOptions(): readonly LoopOption[] {
  return Object.entries(Loop).map(([key, value]) => {
    return { value: key as LoopKey, label: value }
  })
}

function initialLoopConfigValues(): LoopMeasurementConfig {
  return { iterationCount: defaultIterationCount, loopKind: [] }
}

function toLoopArray(loopOption: LoopOption): Loop {
  return loopOption.label
}

export {
  createMultiSelectOptions,
  initialLoopConfigValues,
  toLoopArray,
  defaultIterationCount,
}
