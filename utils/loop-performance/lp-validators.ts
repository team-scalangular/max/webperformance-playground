import {
  Loop,
  LoopConfigValidators,
  LoopErrors,
  LoopMeasurementConfig,
  LoopOption,
} from "@utils/loop-performance/lp-types"
import { EmptyObject } from "@utils/common/types"

function validate(config: LoopMeasurementConfig | EmptyObject): LoopErrors {
  const errors: LoopErrors = {} as LoopErrors
  if (config.loopKind.length < 2) {
    errors.loopKind = LoopConfigValidators.MIN_TWO
  }
  if (config.iterationCount < 100) {
    errors.iterationCount = LoopConfigValidators.MIN_100_ITERATIONS
  }
  return errors
}

function inputsAreValidLoopOptions(
  maybeLoopOptions: unknown
): maybeLoopOptions is LoopOption[] {
  if (Array.isArray(maybeLoopOptions)) {
    return (
      maybeLoopOptions.length === 0 || maybeLoopOptions.every(hasValidLabel)
    )
  }
  return false
}

function hasValidLabel(loopOption: unknown): boolean {
  return Object.values(Loop).includes((loopOption as LoopOption).label)
}

export { validate, inputsAreValidLoopOptions }
