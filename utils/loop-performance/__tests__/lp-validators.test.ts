import {
  Loop,
  LoopConfigValidators,
  LoopMeasurementConfig,
} from "@utils/loop-performance/lp-types"
import { defaultIterationCount } from "@utils/loop-performance/lp-form-handler"
import {
  inputsAreValidLoopOptions,
  validate,
} from "@utils/loop-performance/lp-validators"

const mockConfig = (
  iterationCount = defaultIterationCount,
  loopKind = [Loop.MAP, Loop.WHILE]
): LoopMeasurementConfig => ({
  iterationCount,
  loopKind,
})

describe("lp validators", () => {
  describe("validate", () => {
    it("should return an empty object for a valid config", () => {
      const errors = validate(mockConfig())
      expect(errors).toEqual({})
    })

    describe("given there are less than 2 loops selected", () => {
      it("should return the min two error", () => {
        const errors = validate(mockConfig(defaultIterationCount, []))
        expect(errors).toMatchObject({ loopKind: LoopConfigValidators.MIN_TWO })
      })
    })

    describe("given there are less than 100 iterations selected", () => {
      it("should return an iteration count error", () => {
        const errors = validate(mockConfig(99))
        expect(errors).toMatchObject({
          iterationCount: LoopConfigValidators.MIN_100_ITERATIONS,
        })
      })
    })
  })

  describe("inputs are valid options", () => {
    const mockLoopOptions: unknown[] = [{ value: "FOR", label: Loop.FOR }]

    it("should return true for valid options", () => {
      const result = inputsAreValidLoopOptions(mockLoopOptions)
      expect(result).toBe(true)
    })

    it("should return true for an empty array", () => {
      const result = inputsAreValidLoopOptions([])
      expect(result).toBe(true)
    })

    describe("given the input is not an array", () => {
      it("should return false", () => {
        const result = inputsAreValidLoopOptions({ it: "isWrong" })
        expect(result).toBe(false)
      })
    })

    describe("given the input has label which is included in the available loops", () => {
      it("should return false", () => {
        const result = inputsAreValidLoopOptions([
          { value: "FOR", label: "wrong label" },
        ])
        expect(result).toBe(false)
      })
    })
  })
})
