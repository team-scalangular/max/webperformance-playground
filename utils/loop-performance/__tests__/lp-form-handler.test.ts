import {
  createMultiSelectOptions,
  defaultIterationCount,
  initialLoopConfigValues,
  toLoopArray,
} from "@utils/loop-performance/lp-form-handler"
import {
  Loop,
  LoopMeasurementConfig,
  LoopOption,
} from "@utils/loop-performance/lp-types"

const expectedLoopOptions: LoopOption[] = [
  { label: Loop.FOR, value: "FOR" },
  { label: Loop.FOR_IN, value: "FOR_IN" },
  { label: Loop.FOR_OF, value: "FOR_OF" },
  { label: Loop.FOR_EACH, value: "FOR_EACH" },
  { label: Loop.MAP, value: "MAP" },
  { label: Loop.WHILE, value: "WHILE" },
  { label: Loop.DO_WHILE, value: "DO_WHILE" },
]

describe("lp form handler", () => {
  describe("create multiselect options", () => {
    it("should create the multiselect options from the loop enum", () => {
      const result = createMultiSelectOptions()
      expect(Array.isArray(result))
      expect(result).toEqual(expectedLoopOptions)
    })
  })

  describe("initial loop config values", () => {
    it.each`
      property            | value
      ${"iterationCount"} | ${defaultIterationCount}
      ${"loopKind"}       | ${[]}
    `(
      "should return the correct default value for '$property'",
      ({ value, property }) => {
        const result = initialLoopConfigValues()
        expect(result[property as keyof LoopMeasurementConfig]).toEqual(value)
      }
    )
  })

  describe("to loop array", () => {
    it("should return the label of the loop option", () => {
      const result = toLoopArray(expectedLoopOptions[0])
      expect(result).toEqual(expectedLoopOptions[0].label)
    })
  })
})
